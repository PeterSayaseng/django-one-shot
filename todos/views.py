from django.shortcuts import render, redirect , get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists
    }
    return render(request, "todos/list.html",context)


def todo_list_detail(request,id):
    todo = TodoList.objects.get(id=id)
    context = {
        "todo_list":todo
    }
    todo = get_object_or_404(TodoList, id=id)
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request,id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance = todos)

    context = {
        'todos_form': form,
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")

    return render( request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = ItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = ItemForm(instance = item)

    context = {
        'items_form': form,
    }
    return render(request, 'todos/itemupdate.html', context)
